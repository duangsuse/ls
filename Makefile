build:
	echo building...
	gradle build
build_rs:
	./make_rust.sh
prepare_dir:
	echo Preparing dir...
	mkdir app/libs/x86_64
	mkdir app/libs/arm
	mkdir app/libs/x86
prepare_source:
	echo cloning submodules...
	git submodule update --init
clean:
	gradle clean
	cd libls&&cargo clean
update:
	git pull
	cd libls&git pull
