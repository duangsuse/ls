package org.duangsuse.ls;

import android.app.Activity;
import android.widget.Toast;
import android.os.Bundle;
import org.duangsuse.ls.score;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.loadLibrary("ls");
        Toast.makeText(this, String.valueOf(score.hello()), Toast.LENGTH_SHORT).show();
    }
}
